# Assignment 1 - ReactJS app.

Name: [your name]

## Overview.

[A brief statement on the content of this repository.]

### Features.
[ A bullet-point list of the __new features__ you added to the Movies Fan app (and any modifications to existing features) .]
 
1. topRatedMoviesPage
2. currentPopularMoviesPage
3. languageFilter
4. firebase authentication
5. pagination

## Setup requirements.

[ Outline any non-standard setup steps necessary to run your app locally after cloning the repo.]

## API endpoints.

[ List the __additional__ TMDB endpoints used, giving the description and pathname for each one.] 

e.g.
+ Discover list of movies - discover/movie
+ Movie details - movie/:id
+ Movie genres = /genre/movie/list
+ Top Rated movies = /movie/top_rated
+ Current Popular Movies = /discover/movie


## Routing.

[ List the __new routes__ supported by your app and state the associated page.]

+ /login
+ /movies/top_Rated
+ //movies/currentPopular
+ etc.

[If relevant, state what aspects of your app are protected (i.e. require authentication) and what is public.]

## Independent learning (If relevant).

Itemize the technologies/techniques you researched independently and adopted in your project, 
i.e. aspects not covered in the lectures/labs. Include the source code filenames that illustrate these 
(we do not require code excerpts) and provide references to the online resources that helped you (articles/blogs).